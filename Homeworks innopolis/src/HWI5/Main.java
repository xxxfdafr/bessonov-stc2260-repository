package HWI5;
//Реализовать класс со статическими методами:
//* Сложение всех чисел, которые передаются в метод. Метод возвращает результат сложения.
//* Вычитание всех чисел, которые передаются в метод. Метод возвращает результат вычитания.
//	* Доп. задание: найти наибольшее число из чисел, которые передали в метод. И производить вычитание из него.
//* Умножение всех чисел, которые передаются в метод. Метод возвращает результат умножения.
//* Деление ?всех чисел?, которые передаются в метод. При каждом следующем делении должна идти проверка:
//	*? что делимое число должно быть больше делителя. Если условие не выполнено, то метод возвращает текущий результат деления.
//	*? что делитель - положительное число. Если условие не выполнено, то метод возвращает текущий результат деления.
//* Метод, высчитывающий факториал переданного числа. ?Должна быть проверка, что переданное число положительное.
public class Main {
    public static void main(String[] args) {
        System.out.println(addition(3, 2, 10));
        System.out.println(subtraction(3, 2, 10));
        System.out.println(multiplication(3, 2, 10));
        System.out.println(division(3, 2, 10));
        System.out.println(division(10, 3, 2));
        System.out.println(division(10, -3, 2));
        System.out.println(factorial(4));
        System.out.println(factorial(-4));
    }

    static int addition(int ...number) {
        int result = 0;
        for (int n: number) {
            result += n;
        }
        return result;
    }

    static int subtraction(int ...number) {
        int result = 0;
        int maxNumber = 0;
        for (int i = 0; i < number.length; i++) {
            if (result < number[i]) {
                result = number[i];
                maxNumber = i;
            }
        }
        number[maxNumber] = 0;
        for (int n: number) {
            result -= n;
        }
        return result;
    }

    static int multiplication(int ...number) {
        int result = number[0];
        for (int i = 1; i < number.length;
             i++) {
            result *= number[i];
        }
        return result;
    }

    static double division(int ...number) {
        double result = number[0];
        for (int i = 1; i < number.length; i++) {
            if (result >= number[i] && number[i] > 0) {
                result /= number[i];
            } else {
                return result;
            }
        }
        return result;
    }

    static long factorial(int number) {
        if (number < 0) {
            return 0;
        }
        long result = 1;
        for (int i = 2; i <= number; i++) {
            result *= i;
        }
        return result;
    }
}