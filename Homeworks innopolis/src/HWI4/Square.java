package HWI4;

public class Square extends Rectangle implements Moveable{

    public Square(int x, int y) {
        super(x, y);
    }

    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }
}
