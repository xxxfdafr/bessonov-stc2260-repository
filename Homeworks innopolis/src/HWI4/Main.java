package HWI4;
//Сделать абстрактный класс Figure, у данного класса есть два поля - x и y координаты.
//Классы Ellipse и Rectangle должны быть потомками класса Figure.
//Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
//В классе Figure предусмотреть абстрактный метод getPerimeter().
//Так же, нужно определить интерфейс Moveable c единственным методом .move(int x, int y), который позволит перемещать фигуру на заданные координаты.
//Данный интерфейс должны реализовать только классы Circle и Square.
//В Main создать массив всех фигур и "перемещаемых" фигур. У всех вывести в консоль периметр, а у "перемещаемых" фигур изменить случайным образом координаты.
public class Main {
    public static void main(String[] args) {

        Ellipse ellipse = new Ellipse(10, 5);
        Rectangle rectangle = new Rectangle(8, 5);
        Square square = new Square(3, 3);
        Circle circle = new Circle(5, 5);

        Figure[] figure = new Figure[4];
        figure[0] = ellipse;
        figure[1] = rectangle;
        figure[2] = square;
        figure[3] = circle;

        for (Figure value : figure) {
            System.out.println(value.getPerimeter());
        }
        System.out.println(square);
        System.out.println(circle);
        square.move((int) (Math.random() * 10), (int) (Math.random() * 10));
        ((Moveable)circle).move((int) (Math.random() * 10), (int) (Math.random() * 10));
        System.out.println(square);
        System.out.println(circle);
    }
}
