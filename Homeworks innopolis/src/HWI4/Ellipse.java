package HWI4;

public class Ellipse extends Figure{

    public Ellipse(int x, int y) {
        super(x, y);
    }

    @Override
    public double getPerimeter() {
        double a = (double) getX() / 2;
        double b = (double) getY() / 2;
        return 4 * (Math.PI * a * b + (a - b) * (a - b)) / (a + b);
    }
}
