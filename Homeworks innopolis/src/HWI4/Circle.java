package HWI4;

public class Circle extends Ellipse implements Moveable{

    public Circle(int x, int y) {
        super(x, y);
    }

    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }
}
