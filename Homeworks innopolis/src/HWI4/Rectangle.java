package HWI4;

public class Rectangle extends Figure{

    public Rectangle(int x, int y) {
        super(x, y);
    }

    @Override
    public double getPerimeter() {
        return 2 * (getX() + getY());
    }
}
