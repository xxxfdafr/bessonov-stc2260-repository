package HWI7;
//Создать класс Human у которого будут поля:
//
//private String name;
//private String lastName;
//private String patronymic;
//private String city;
//private String street;
//private String house;
//private String flat;
//private String numberPassport;
//
//Переопределить три метода: toString(), hashCode(), equals
//Метод toString должен выводить информацию таким образом (пример):
//Пупкин Вася Варфаламеевич
//Паспорт:
//Серия: 98 22 Номер: 897643
//Город Джава, ул. Программистов, дом 15, квартира 54
//Метод equals() должен сравнивать людей по номеру паспорта
public class Main {
    public static void main(String[] args) {
        Human human = new Human("Вася", "Пупкин", "Варфаламеевич", "Джава", "Программистов", "12", "54","9822897643");
        Human human2 = new Human("Вася", "Пупкин", "Варфаламеевич", "Джава", "Программистов", "12", "54","9999999999");
        Human human3 = new Human("Вася", "Пупкин", "Варфаламеевич", "Джава", "Программистов", "12", "54","9822897643");
        System.out.println(human.equals(human2));
        System.out.println(human.equals(human3));
        System.out.println(human.toString());
    }
}
