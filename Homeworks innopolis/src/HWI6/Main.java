package HWI6;
//В main в качестве condition подставить:
//проверку на четность элемента
//проверку, является ли сумма цифр элемента четным числом.
//Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
//Доп. задача: проверка на палиндромность числа (число читается одинаково и слева, и справа -> 11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).
public class Main {
    public static void main(String[] args) {
        int[] array = {10, 20,12, 33};
        array = Sequence.filter(array, number -> number % 2 == 0);
        for (int j : array) {
            System.out.print("[" + j + "]");
        }
        System.out.println();
//--------------------------------------------------------------
        array = new int[]{1032, 2052, 1256, 3332};
        array = Sequence.filter(array, number -> {
            int result = 0;
            while (number != 0) {
                result += number % 10;
                number /= 10;
            }
            return result % 2 == 0;
        });
        for (int j : array) {
            System.out.print("[" + j + "]");
        }
        System.out.println();
//--------------------------------------------------------------
        array = new int[]{1032, 2842, 2256, 3332};
        array = Sequence.filter(array, number -> {
            boolean result = false;
            while (number != 0) {
                if (number % 2 == 0) {
                    result = true;
                } else {
                    return false;
                }
                number /= 10;
            }
            return result;
        });
        for (int j : array) {
            System.out.print("[" + j + "]");
        }
        System.out.println();
//--------------------------------------------------------------
        array = new int[]{2032, 2052, 1256, 3223};
        array = Sequence.filter(array, number -> {
            char[] numbers = String.valueOf(number).toCharArray();
            boolean result = false;
            int i = 0;
            int j = numbers.length - 1;
            while (i < j) {
                if (numbers[i] == numbers[j]) {
                    result = true;
                } else {
                    return false;
                }
                j--;
                i++;
            }
            return result;
        });
        for (int j : array) {
            System.out.print("[" + j + "]");
        }
        System.out.println();
    }
}
