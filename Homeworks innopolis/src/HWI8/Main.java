package HWI8;

public class Main {
    public static void main(String[] args){
        User user = UsersRepositoryFileImpl.findById(1);
        user.setName("Владимир");
        user.setAge(27);
        UsersRepositoryFileImpl.update(user);
        User user1 = new User("Иван", "Иванов", 23, false);
        UsersRepositoryFileImpl.create(user1);
        UsersRepositoryFileImpl.delete(3);
    }
}
