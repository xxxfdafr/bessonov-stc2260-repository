package HWI8;

import java.io.*;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl {
    private static final String FILE_NAME = "Homeworks innopolis\\src\\HWI8\\Users.txt";
    private static ArrayList<User> users;

    private static void write (){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(FILE_NAME));
            for (User user : users) {
                bw.write(user.toString() + "\n");
            }
            bw.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static {
        try {
            BufferedReader br = new BufferedReader(new FileReader(FILE_NAME));
            users = (ArrayList<User>) br.lines()
                    .map(stroke -> {
                        String[] info = stroke.split("\\|");
                        return new User(Integer.parseInt(info[0]), info[1], info[2], Integer.parseInt(info[3]), Boolean.parseBoolean(info[4]));
                    })
                    .collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static User findById(int id) {
        return users.stream()
                .filter(user1 -> user1.getId() == id)
                .findAny()
                .orElse(null);
    }

    public static void create(User user) {
        Integer id = users.stream()
                .map(User::getId)
                .max(Integer::compareTo)
                .orElse(null);
        if (id == null) {
            id = 0;
        }
        user.setId(++id);
        users.add(new User(user.getId(), user.getName(), user.getSurname(), user.getAge(), user.isHasWork()));
        write();
    }

    public static void update(User user) {
        users = (ArrayList<User>) users.stream()
                .map(user1 -> {
                    if (user1.getId() == user.getId()) {
                        return user;
                    }
                    return user1;
                })
                .collect(Collectors.toList());
        write();
    }

    public static void delete(int id) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == id) {
                users.remove(i);
            }
        }
        write();
    }
}