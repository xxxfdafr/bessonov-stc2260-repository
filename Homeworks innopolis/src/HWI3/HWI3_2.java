package HWI3;

import java.util.Arrays;

public class HWI3_2 {
    //Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые.
    public static void main(String[] args) {
        int[] array = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        System.out.println("Было: " + Arrays.toString(array));
        sort(array);
        System.out.println("Стало: " + Arrays.toString(array));
    }
    public static void sort(int[] array) {
        int j = 1;
        for (int i = 0; i < array.length; i++) {
            for (; array[i] == 0; j++) {
                if (j == array.length) {
                    return;
                } else if (array[j] != 0) {
                    array[i] = array[j];
                    array[j] = 0;
                    j++;
                    break;
                }
            }
        }
    }
}