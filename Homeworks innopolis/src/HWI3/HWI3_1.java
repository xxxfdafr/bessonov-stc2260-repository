package HWI3;

import java.util.Scanner;

public class HWI3_1 {
    //Реализовать функцию, принимающую на вход массив и целое число.
    //Данная функция должна вернуть индекс этого числа в массиве.
    //Если число в массиве отсутствует - вернуть -1.
    public static void main(String[] args) {
        int[] array = {5, 7, 6 ,9 ,4};
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите искомое число: ");
        int numberSearch = scanner.nextInt();
        int numberPlace = search(array,numberSearch);
        if (numberPlace != -1) {
            System.out.println("Место искомого числа: " + numberPlace);
        } else {
            System.out.println("Число не найдено!");
        }
    }
    public static int search(int[] array, int numberSearch) {
        int numberPlace = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == numberSearch) {
                numberPlace = i;
            }
        }
        return numberPlace;
    }
}