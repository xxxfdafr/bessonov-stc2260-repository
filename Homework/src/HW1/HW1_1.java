package HW1;

import java.util.Scanner;
public class HW1_1 {
    //Написать программу, которая высчитывает наибольшую цифру в числе.
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int maxNumber = 0;
        while (number != 0) {
            int temp = number % 10;
            if (temp > maxNumber) {
                maxNumber = temp;
            }
            number /= 10;
        }
        System.out.println("Max number = " + maxNumber);
    }
}