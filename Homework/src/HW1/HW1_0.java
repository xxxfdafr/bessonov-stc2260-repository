package HW1;

public class HW1_0 {
    //Расписать диапазоны значений примитивов (byte, short, int, long, float, double, char, boolean)
    public static void main(String[] args) {
        System.out.println("byte от " + Byte.MIN_VALUE + " до " + Byte.MAX_VALUE);
        System.out.println("short от " + Short.MIN_VALUE + " до " + Short.MAX_VALUE);
        System.out.println("int от " + Integer.MIN_VALUE + " до " + Integer.MAX_VALUE);
        System.out.println("long от " + Long.MIN_VALUE + " до " + Long.MAX_VALUE);
        System.out.println("float от " + Float.MIN_VALUE + " до " + Float.MAX_VALUE);
        System.out.println("double от " + Double.MIN_VALUE + " до " + Double.MAX_VALUE);
        System.out.println("char от 0 до 65,535");
        System.out.println("boolean false,true");
        /*
         *\b  Backspace (Курсор двигается на одно место влево)
         *\n  Newline (Новая строчка)
         *\f  Newpage (Новая страница)
         *\r  Carriage return (Курсор становится на первое место в строчке, возврат каретки)
         *\t  Горизонтальный табулятор
         *\"  Двойная кавычка
         *\'  Одинарная кавычка
         *\\  Backslash(вызов обратного слэша)
         *\304  Символы с октальным значением от 000 до 377, например \304 соответствует символу Ä
         *\u00C4 Вызов символа уникода (Unicode-16). Где например \u00C4 соответствует Ä
         */
    }
}
