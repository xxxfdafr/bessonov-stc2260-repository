package HW0;

import java.util.Scanner;
//Для входной последовательности целых положительных чисел, оканчивающейся на -1,
//необходимо вывести количество четных чисел и произведение всех нечетных чисел.
public class HW0 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int numberOfEvenNumbers = 0;
        int productOfOddNumbers = 0;
        while (number != -1) {
            if (number % 2 == 0) {
                numberOfEvenNumbers ++;
            } else if (number % 2 != 0) {
                if (productOfOddNumbers == 0) {
                    productOfOddNumbers = number;
                } else {
                    productOfOddNumbers *= number;
                }
            }
            number = scanner.nextInt();
        }
        System.out.println("Number of even numbers = " + numberOfEvenNumbers);
        System.out.println("Product of all odd numbers = " + productOfOddNumbers);
    }
}

