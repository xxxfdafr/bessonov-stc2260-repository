package ManyToMany;

import java.sql.*;
import java.util.Scanner;

public class Main {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "password";

    public static void main(String[] args) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        String post = null;
        do {
            System.out.print("Студент или учитель Y|N: ");
            String yn = scanner.next();
            if (yn.equals("y")||yn.equals("Y")||yn.equals("у")||yn.equals("У")) {
                post = "student";
            } else if (yn.equals("n")||yn.equals("N")||yn.equals("н")||yn.equals("Н")) {
                post = "t";
            }
        } while (post == null);
        System.out.print("Фамилия: ");
        String name = scanner.next();

        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(
                "select t.name as teacher_name, student.name as student_name\n" +
                        "from student\n" +
                        "         join teacher_and_student tas on student.id = tas.student_id\n" +
                        "         join teacher t on t.id = tas.teacher_id\n" +
                        "where " + post + ".name like '" + name + "%'");

        if (post.equals("student")) System.out.println("Учетиля студента:");
        if (post.equals("t")) System.out.println("Ученики учителя:");
        while (result.next()) {
            System.out.println(result.getString("teacher_name"));
        }
    }
}
