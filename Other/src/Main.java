public class Main {
    public static void main(String[] args) {
        System.out.print("Программа решает квадратное уравнение вида: ");
        System.out.println("ax^2 + bx + c = 0");

        for (int i = 0; i < 100; i++) {
            SquareEquation squareEquation1 = new SquareEquation(i, 3, 1);
            squareEquation1.calculate();

            squareEquation1.printResult();

        }

    }
}
