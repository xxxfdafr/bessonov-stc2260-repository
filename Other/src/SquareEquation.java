public class SquareEquation {

    private double a, b, c, D, x, x1, x2;
    private String result = "Произведите рассчет";

    public SquareEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public void calculate() {
        D= Math.pow(this.b, 2) - 4 * this.a * this.c;
        double twoA = 2 * this.a;
        if (D > 0) {
            double sqrt = Math.sqrt(D);
            x1 = (-this.b - sqrt) / (twoA);
            x2 = (-this.b + sqrt) / (twoA);
            result = "Корни уравнения: x1 = " + x1 + ", x2 = " + x2;
        } else if (D == 0) {
            x = -this.b / (twoA);
            result = "Уравнение имеет единственный корень: x = " + x;
        }
        else {
            result = "Уравнение не имеет действительных корней!";
        }
    }

    public void printResult() {
        System.out.println(result);
    }
}
