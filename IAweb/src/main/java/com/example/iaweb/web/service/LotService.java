package com.example.iaweb.web.service;

import com.example.iaweb.web.dto.LotForm;
import com.example.iaweb.web.model.Lot;

import java.util.List;

public interface LotService {

    void lots(LotForm dto);

    List<Lot> getAllLot();
    void displayLot(Long lotId);
    void removeLot(Long lotId);
    void deleteLot(Long lotId);
}
