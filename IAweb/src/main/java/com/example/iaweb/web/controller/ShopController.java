package com.example.iaweb.web.controller;

import com.example.iaweb.web.model.Lot;
import com.example.iaweb.web.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/shop")
public class ShopController {

    private final ShopService shopService;

    @Autowired
    public ShopController(ShopService shopService) {
        this.shopService = shopService;
    }

    @GetMapping
    public String getAllLot(Model model) {
        List<Lot> lots = shopService.getAllLot();
        model.addAttribute("lots", lots);
        List<Lot> basket = shopService.getAllBasket();
        model.addAttribute("basket", basket);
        return "shop";
    }

    @PostMapping(value = "/{lot-id}/add")
    public String addBasket(@PathVariable("lot-id") Long lotId) {
        shopService.addBasket(lotId);
        return "redirect:/shop";
    }
    @PostMapping(value = "/{lot-id}/remove")
    public String removeBasket(@PathVariable("lot-id") Long lotId) {
        shopService.removeBasket(lotId);
        return "redirect:/shop";
    }

    @PostMapping(value = "/order")
    public String clearBasket() {
        shopService.clearAllBasket();
        return "redirect:/shop";
    }
}
