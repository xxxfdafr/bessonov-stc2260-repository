package com.example.iaweb.web.service;

import com.example.iaweb.web.model.Account;
import com.example.iaweb.web.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    private static String noId(){
        throw new UsernameNotFoundException("Аккаунт с таким id не существует");
    }

    @Override
    public void confirmedAccount(Long accountId) {
        Optional<Account> accountOpt = accountRepository.findById(accountId);
        if (!accountOpt.isPresent()) {
            noId();
        }
        Account account = accountOpt.get();
        account.setState(Account.State.CONFIRMED);
        accountRepository.save(account);
    }
    @Override
    public void banAccount(Long accountId) {
        Optional<Account> accountOpt = accountRepository.findById(accountId);
        if (!accountOpt.isPresent()) {
            noId();
        }
        Account account = accountOpt.get();
        account.setState(Account.State.BANNED);
        accountRepository.save(account);
    }
    @Override
    public void deleteAccount(Long accountId) {
        Optional<Account> accountOpt = accountRepository.findById(accountId);
        if (!accountOpt.isPresent()) {
            noId();
        }
        Account account = accountOpt.get();
        account.setState(Account.State.DELETED);
        accountRepository.save(account);
    }
    @Override
    public void setUserAccount(Long accountId) {
        Optional<Account> accountOpt = accountRepository.findById(accountId);
        if (!accountOpt.isPresent()) {
            noId();
        }
        Account account = accountOpt.get();
        account.setRole(Account.Role.USER);
        accountRepository.save(account);
    }
    @Override
    public void setAdminAccount(Long accountId) {
        Optional<Account> accountOpt = accountRepository.findById(accountId);
        if (!accountOpt.isPresent()) {
            noId();
        }
        Account account = accountOpt.get();
        account.setRole(Account.Role.ADMIN);
        accountRepository.save(account);
    }
}
