package com.example.iaweb.web.repositories;

import com.example.iaweb.web.model.Lot;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BasketRepository {
    private final List<Lot> basket;

    public BasketRepository(List<Lot> basket) {
        this.basket = basket;
    }

    public List<Lot> getBasket() {
        return basket;
    }

    public void addBasket(Lot lot) {
        basket.add(lot);
    }

    public void delLotBasket(Lot lot) {
        for (int i = 0; i < basket.size(); i++) {
            if (basket.get(i).getId() == lot.getId()) {
                basket.remove(i);
            }
        }
    }

    public void clearBasket() {
        basket.clear();
    }
}
