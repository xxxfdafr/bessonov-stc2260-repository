package com.example.iaweb.web.service;

import com.example.iaweb.web.model.Lot;

import java.util.List;

public interface ShopService {
    List<Lot> getAllLot();

    List<Lot> getAllBasket();

    void addBasket(Long lotId);
    void removeBasket(Long lotId);

    void clearAllBasket();
}
