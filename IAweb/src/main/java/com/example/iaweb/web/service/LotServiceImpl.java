package com.example.iaweb.web.service;

import com.example.iaweb.web.dto.LotForm;
import com.example.iaweb.web.model.Lot;
import com.example.iaweb.web.repositories.LotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LotServiceImpl implements LotService{

    private final LotRepository lotRepository;

    @Autowired
    public LotServiceImpl(LotRepository lotRepository) {
        this.lotRepository = lotRepository;
    }

    @Override
    public void lots(LotForm dto) {
        Lot lot = Lot.builder()
                .lotName(dto.getLotName())
                .lotPrice(dto.getLotPrice())
                .state(Lot.State.NOT_DISPLAYED)
                .build();

        lotRepository.save(lot);
    }

    @Override
    public List<Lot> getAllLot() {
        return lotRepository.findAll();
    }

    private static String noId(){
        throw new UsernameNotFoundException("Лот с таким id не существует");
    }

    @Override
    public void displayLot(Long lotId) {
        Optional<Lot> lotOpt = lotRepository.findById(lotId);
        if (!lotOpt.isPresent()) {
            noId();
        }
        Lot lot = lotOpt.get();
        lot.setState(Lot.State.DISPLAYED);
        lotRepository.save(lot);
    }
    @Override
    public void removeLot(Long lotId) {
        Optional<Lot> lotOpt = lotRepository.findById(lotId);
        if (!lotOpt.isPresent()) {
            noId();
        }
        Lot lot = lotOpt.get();
        lot.setState(Lot.State.NOT_DISPLAYED);
        lotRepository.save(lot);
    }
    @Override
    public void deleteLot(Long lotId) {
        Optional<Lot> lotOpt = lotRepository.findById(lotId);
        if (!lotOpt.isPresent()) {
            noId();
        }
        Lot lot = lotOpt.get();
        lot.setState(Lot.State.DELETED);
        lotRepository.save(lot);
    }
}
