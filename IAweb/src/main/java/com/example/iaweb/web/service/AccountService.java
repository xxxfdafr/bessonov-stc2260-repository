package com.example.iaweb.web.service;

import com.example.iaweb.web.model.Account;

import java.util.List;

public interface AccountService {

    List<Account> getAllAccounts();

    void confirmedAccount(Long accountId);

    void banAccount(Long accountId);

    void deleteAccount(Long accountId);

    void setUserAccount(Long accountId);

    void setAdminAccount(Long accountId);
}
