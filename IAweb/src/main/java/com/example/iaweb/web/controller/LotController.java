package com.example.iaweb.web.controller;

import com.example.iaweb.web.dto.LotForm;
import com.example.iaweb.web.model.Lot;
import com.example.iaweb.web.service.LotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/lots")
public class LotController {

    private final LotService lotService;

    @Autowired
    public LotController(LotService lotService) {
        this.lotService = lotService;
    }

    @GetMapping
    public String getAllLot(Model model) {
        List<Lot> lots = lotService.getAllLot();
        model.addAttribute("lots", lots);
        model.addAttribute("lotForm", new LotForm());
        return "lots";
    }

    @PostMapping(value = "/{lot-id}/display")
    public String displayLot(@PathVariable("lot-id") Long lotId) {
        lotService.displayLot(lotId);
        return "redirect:/lots";
    }
    @PostMapping(value = "/{lot-id}/remove")
    public String removeLot(@PathVariable("lot-id") Long lotId) {
        lotService.removeLot(lotId);
        return "redirect:/lots";
    }
    @PostMapping(value = "/{lot-id}/delete")
    public String deleteLot(@PathVariable("lot-id") Long lotId) {
        lotService.deleteLot(lotId);
        return "redirect:/lots";
    }
    @PostMapping
    public String addLots(@Valid LotForm dto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(dto);
            return "lots";
        }

        lotService.lots(dto);
        return "redirect:/lots";
    }
}
