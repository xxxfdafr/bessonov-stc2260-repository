package com.example.iaweb.web.service;


import com.example.iaweb.web.dto.SignUpForm;

public interface SignUpService {

    void signUp(SignUpForm dto);
}
