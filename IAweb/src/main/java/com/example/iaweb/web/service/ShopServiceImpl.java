package com.example.iaweb.web.service;

import com.example.iaweb.web.repositories.BasketRepository;
import com.example.iaweb.web.model.Lot;
import com.example.iaweb.web.repositories.LotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShopServiceImpl implements ShopService {

    private final LotRepository lotRepository;
    private final BasketRepository basket;

    @Autowired
    public ShopServiceImpl(LotRepository lotRepository, BasketRepository basket) {
        this.lotRepository = lotRepository;
        this.basket = basket;
    }

    private static String noId(){
        throw new UsernameNotFoundException("Лот с таким id не существует");
    }

    @Override
    public List<Lot> getAllLot() {
        return lotRepository.findAllByState(Lot.State.DISPLAYED);
    }

    @Override
    public List<Lot> getAllBasket() {
        return basket.getBasket();
    }

    @Override
    public void addBasket(Long lotId) {
        Optional<Lot> lotOpt = lotRepository.findById(lotId);
        if (!lotOpt.isPresent()) {
            noId();
        }
        Lot lot = lotOpt.get();
        basket.addBasket(lot);
    }

    @Override
    public void removeBasket(Long lotId) {
        Optional<Lot> lotOpt = lotRepository.findById(lotId);
        if (!lotOpt.isPresent()) {
            noId();
        }
        Lot lot = lotOpt.get();
        basket.delLotBasket(lot);
    }

    @Override
    public void clearAllBasket() {
        basket.clearBasket();
    }
}
