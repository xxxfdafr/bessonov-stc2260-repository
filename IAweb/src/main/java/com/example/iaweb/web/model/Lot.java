package com.example.iaweb.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Lot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "lot_name")
    private String lotName;
    @Column(name = "lot_price")
    private Float lotPrice;
    @Enumerated(value = EnumType.STRING)
    private State state;

    public enum State {
        NOT_DISPLAYED, DISPLAYED, DELETED
    }
}
