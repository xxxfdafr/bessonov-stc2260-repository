package com.example.iaweb.web.repositories;

import com.example.iaweb.web.model.Lot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LotRepository extends JpaRepository<Lot, Long> {

    List<Lot> findAllByState(Lot.State state);
}
