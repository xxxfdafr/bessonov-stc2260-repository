package com.example.iaweb.web.controller;

import com.example.iaweb.web.model.Account;
import com.example.iaweb.web.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Controller
@RequestMapping("/accounts")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }
    //http:localhost:8080/accounts
    @GetMapping
    public String getAllAccounts(Model model) {
        List<Account> accounts = accountService.getAllAccounts();
        model.addAttribute("accounts", accounts);
        return "accounts";
    }

    @PostMapping(value = "/{account-id}/confirmed")
    public String confirmedAccount(@PathVariable("account-id") Long accountId) {
        accountService.confirmedAccount(accountId);
        return "redirect:/accounts";
    }
    @PostMapping(value = "/{account-id}/ban")
    public String banAccount(@PathVariable("account-id") Long accountId) {
        accountService.banAccount(accountId);
        return "redirect:/accounts";
    }
    @PostMapping(value = "/{account-id}/delete")
    public String deleteAccount(@PathVariable("account-id") Long accountId) {
        accountService.deleteAccount(accountId);
        return "redirect:/accounts";
    }
    @PostMapping(value = "/{account-id}/set_user")
    public String setUserAccount(@PathVariable("account-id") Long accountId) {
        accountService.setUserAccount(accountId);
        return "redirect:/accounts";
    }
    @PostMapping(value = "/{account-id}/set_admin")
    public String setAdmibAccount(@PathVariable("account-id") Long accountId) {
        accountService.setAdminAccount(accountId);
        return "redirect:/accounts";
    }
}
