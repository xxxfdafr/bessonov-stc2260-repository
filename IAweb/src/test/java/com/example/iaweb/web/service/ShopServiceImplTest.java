package com.example.iaweb.web.service;

import com.example.iaweb.web.model.Lot;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ShopServiceImplTest {

    @Autowired
    private ShopService shopService;

    @Test
    void getAllLot() {
        List<Lot> lots =  shopService.getAllLot();
        for (int i = 0; i < lots.size(); i++) {
            if (lots.get(i).getState() != Lot.State.DISPLAYED) {
                throw new AssertionError("ShopServiceImpl.getAllLot Lot.State find NO DISPLAYED");
            }
        }
    }
}